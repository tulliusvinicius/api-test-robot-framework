*** Settings ***
Resource    ../../resources/keywords/api_pet.robot


*** Test Cases ***

TC002: Add a new pet to the store with success - HTTP 200 esperado
    [Tags]    teste
    # Dado que o usuário tenha um novo PET com nome "Garfiu"
    # Quando o usuário enviar uma solicitação POST para "/pet" com os detalhes do PET
    Quando o teste for um POST para "/pet" com os detalhes do PET
    Então a resposta deve ter o código de status 200
    E o cadastro do Pet é realizado com sucesso

TC003: Update an existing pet with success - HTTP 200 esperado
    # Dado que exista um PET com ID 123
    Quando o usuário enviar uma solicitação PUT para "/pet" com os detalhes do PET
    Então a resposta deve ter o código de status 200
    # E o cadastro do Pet atualizado com sucesso

TC004: Finds pet by status Available with success - HTTP 200 esperado
    Quando o usuário enviar uma solicitação GET para "/pet/findByStatus" com status "available"
    Então a resposta deve ter o código de status 200
    # E a resposta deve incluir uma lista de PETs com status "available"

TC005: Finds pet by status Pending with success - HTTP 200 esperado
    Quando o usuário enviar uma solicitação GET para "/pet/findByStatus" com status "pending"
    Então a resposta deve ter o código de status 200
    # E a resposta deve incluir uma lista de PETs com status "pending"

TC006: Finds pet by status Sold with success - HTTP 200 esperado
    Quando o usuário enviar uma solicitação GET para "/pet/findByStatus" com status "sold"
    Então a resposta deve ter o código de status 200
    # E a resposta deve incluir uma lista de PETs com status "sold"

TC007: Finds pet by ID with success - HTTP 200 esperado
    # Dado que exista um PET com ID 123
    Quando o usuário enviar uma solicitação GET para "/pet/9223372036854616694"
    Então a resposta deve ter o código de status 200
    # E a resposta deve incluir os detalhes do PET com ID 123

TC008: Finds pet by ID with errors - HTTP 404 esperado
    # Dado que não exista um PET com ID 1231232131321
    Quando o usuário enviar uma solicitação GET para "/pet/1231232131321"
    Então a resposta deve ter o código de status 404
    # E a resposta deve retornar a mensagem: "Pet not found"

