[Documentation]    Configurações das APIs PetStore
*** Variables ***

# HEADERS
${accept}               application/json
${content-Type}         application/json

# BASE
${base_url}                      https://petstore.swagger.io/v2
${alias}                         PetStore

###   ENDPOINTS   ###

# API /PET
${endpoint_pet}                  /pet
${endpoint_pet_find_by_status}   /pet/findByStatus?status=