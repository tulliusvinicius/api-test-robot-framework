*** Settings ***
Documentation        Testes da API /pet
Library              String
Library              Collections
Library              RequestsLibrary
Resource             ../../configs/api_config.robot
Resource             ../../resources/utils/utils.robot

*** Keywords ***
Criar sessão na petstore
    ${HEADERS}    Create Dictionary    Accept=${accept}    Content-Type=${Content-Type}
    Create Session    alias=${alias}    url=${base_url}    headers=${HEADERS}    disable_warnings=True

Quando o usuário enviar uma solicitação ${REQUEST} para "/pet" com os detalhes do PET
    # Criando dicionário da chave category
    ${category}    Create Dictionary    id=0    name=Cat
    # Criando lista da chave photoUrls
    ${photo_url}   Create List          string
    # Criando dicionário da chave tags
    ${tags}        Create Dictionary    id=0    name=string
    # Criando lista da chave tags
    ${tag_list}    Create List          ${tags}
    
    # Criando body da requisição
    ${BODY}        Create Dictionary    
    ...            id=0    
    ...            category=${category}
    ...            name=Garfiu
    ...            photoUrls=${photo_url}
    ...            tags=${tag_list}
    ...            status=available
    
    # Criando sessão e enviando POST
    Criar sessão na petstore
    
    # Verificando se a requisição será um POST ou PUT
    IF          $REQUEST == 'POST'
        ${RESPONSE}    POST On Session    
        ...            alias=${alias}
        ...            url=${endpoint_pet}
        ...            json=${BODY}

    ELSE IF     $REQUEST == 'PUT'
        ${RESPONSE}    PUT On Session    
        ...            alias=${alias}
        ...            url=${endpoint_pet}
        ...            json=${BODY}
    END
    
    # Armazendo resposta na variável para utilizar em outras keywords
    Set Test Variable                 ${RESPONSE_JSON}    ${RESPONSE.json()}   #  interpreta a resposta como um objeto JSON e retorna um dicionário Python. Neste dicionário, as strings são representadas com aspas simples ('), seguindo a convenção do Python.
    # Armazendo resposta na variável para gerar arquivo json
    Set Test Variable                 ${RESPONSE_TEXT}    ${RESPONSE.text}     #  retorna a resposta como uma string, mantendo as aspas originais do JSON, que são aspas duplas (").
    Criar arquivo Json do response    ${RESPONSE_TEXT}
    Receber arquivo Json como entrada    ${OUTPUT_DIR}/teste.json
    Comparar os Json dos responses

Então a resposta deve ter o código de status ${STATUS_CODE}
    Status Should Be    ${STATUS_CODE}

E o cadastro do Pet é realizado com sucesso
    Dictionary Should Contain Key     ${RESPONSE_JSON}              id
    Dictionary Should Contain Item    ${RESPONSE_JSON}[category]    name    Cocktail
    Dictionary Should Contain Item    ${RESPONSE_JSON}              name    Meg
    Dictionary Should Contain Item    ${RESPONSE_JSON}              status  available

Quando o usuário enviar uma solicitação GET para "/pet/findByStatus" com status "${by_status}"
    # Criando sessão e enviando GET
    Criar sessão na petstore
    ${RESPONSE}    GET On Session    
    ...            alias=${alias} 
    ...            url=${endpoint_pet_find_by_status}${by_status}
    
    # Armazenando resposta na variável para utilizar em outras keywords
    Set Test Variable    ${RESPONSE_JSON}    ${RESPONSE.json()}

# E a resposta deve incluir uma lista de PETs com status "${by_status}"

Quando o usuário enviar uma solicitação GET para "/pet/${id_pet}"
    Criar sessão na petstore
    ${RESPONSE}    GET On Session    
    ...            alias=${alias}
    ...            url=${endpoint_pet}/${id_pet}
    ...            expected_status=any
    
    Set Test Variable    ${RESPONSE_JSON}    ${RESPONSE.json()}


Quando o teste for um ${REQUEST} para "/pet" com os detalhes do PET
    # Criando dicionário da chave category
    ${category}    Create Dictionary    id=0    name=Cat
    # Criando lista da chave photoUrls
    ${photo_url}   Create List          string
    # Criando dicionário da chave tags
    ${tags}        Create Dictionary    id=0    name=string
    # Criando lista da chave tags
    ${tag_list}    Create List          ${tags}
    
    # Criando body da requisição
    ${BODY}    Format String    ${EXECDIR}/resources/data/input/api_pet/POST_add_a_new_pet_to_the_store.json 
    ...                         category_name=Cocktail
    ...                         pet_name=Meg
    ...                         tags_name=Amarela
    ...                         pet_status=available
    # Log    Body:\n${BODY}
    
    # Criando sessão e enviando POST
    Criar sessão na petstore
    
    # Verificando se a requisição será um POST ou PUT
    IF          $REQUEST == 'POST'
        ${RESPONSE}    POST On Session    
        ...            alias=${alias}
        ...            url=${endpoint_pet}
        ...            data=${BODY}

    ELSE IF     $REQUEST == 'PUT'
        ${RESPONSE}    PUT On Session    
        ...            alias=${alias}
        ...            url=${endpoint_pet}
        ...            json=${BODY}
    END
    
    # Armazendo resposta na variável para utilizar em outras keywords
    Set Test Variable                 ${RESPONSE_JSON}    ${RESPONSE.json()}   #  interpreta a resposta como um objeto JSON e retorna um dicionário Python. Neste dicionário, as strings são representadas com aspas simples ('), seguindo a convenção do Python.
    # Armazendo resposta na variável para gerar arquivo json
    Set Test Variable                 ${RESPONSE_TEXT}    ${RESPONSE.text}     #  retorna a resposta como uma string, mantendo as aspas originais do JSON, que são aspas duplas (").
    Criar arquivo Json do response    ${RESPONSE_TEXT}
    # Receber arquivo Json como entrada    ${OUTPUT_DIR}/teste.json
    Comparar os Json dos responses
