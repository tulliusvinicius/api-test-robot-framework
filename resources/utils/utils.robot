*** Settings ***
Library        json
Library        OperatingSystem
Library        Collections

*** Variables ***
${OUTPUT_DIR}    ./resources/data/output

*** Keywords ***

Criar arquivo Json do response
    [Arguments]       ${response_text}
    ${json_string}    Convert To String                  ${response_text}
    Create File       ${OUTPUT_DIR}/teste.json           ${json_string}

Receber arquivo Json como entrada
    [Arguments]    ${json_file}
    ${json_file}    Get File    ${json_file}
    ${json_file}    Evaluate    json.loads($json_file)
    RETURN    ${json_file}
    

Comparar os Json dos responses
    ${teste}    Receber arquivo Json como entrada    ${OUTPUT_DIR}/teste.json
    ${teste2}    Receber arquivo Json como entrada    ${OUTPUT_DIR}/teste.json
    Dictionaries Should Be Equal    ${teste}    ${teste2}
    Log    Recebi o ${teste}
    Log    Recebi o ${teste2}